#pragma once
#include <iostream>



class TicTacToe
{

private:
	//Fields

	char m_board[9];

	int m_numTurns;

	char m_playerTurn;

	char m_winner;

	char m_player1 = { *"X" };

	char m_player2 = { *"O" };

public:

	TicTacToe()
	{
		m_numTurns = 1;
		m_playerTurn = GetPlayerTurn();
		
		//sets gameboard playspace numbers on run
		m_board[0] = *"1";
		m_board[1] = *"2";
		m_board[2] = *"3";
		m_board[3] = *"4";
		m_board[4] = *"5";
		m_board[5] = *"6";
		m_board[6] = *"7";
		m_board[7] = *"8";
		m_board[8] = *"9";
	}




	//Methods

	void DisplayBoard()
	{
		//Output board to console
		GetPlayerTurn();

		std::cout << "   " << m_board[0] << " | " << m_board[1] << " | " << m_board[2] << "\n ----------- \n" << "   " << m_board[3] << " | " << m_board[4] << " | " << m_board[5] << "\n ----------- \n" << "   " << m_board[6] << " | " << m_board[7] << " | " << m_board[8] << "\n";

	}

	//checks if there are any three in a row matches for a winner and if not any, no player is set as m_winner
	void CheckWinner()
	{
	//1
		if (m_board[0] == m_board[1] && m_board[1] == m_board[2])
				{
					m_winner = m_board[0];
				}
	//2
		else if (m_board[3] == m_board[4] && m_board[4] == m_board[5])
				{
					m_winner = m_board[3];
				}
	//3
		else if (m_board[6] == m_board[7] && m_board[7] == m_board[8])
				{
					m_winner = m_board[6];
				}
	//4
		else if (m_board[0] == m_board[3] && m_board[3] == m_board[6])
				{
					m_winner = m_board[0];
				}
	//5
		else if (m_board[1] == m_board[4] && m_board[4] == m_board[7])
				{
					m_winner = m_board[1];
				}
	//6
		else if (m_board[2] == m_board[5] && m_board[5] == m_board[8])
				{
					m_winner = m_board[2];
				}
	//7
		else if (m_board[0] == m_board[4] && m_board[4] == m_board[8])
				{
					m_winner = m_board[0];
				}
	//8
		else if (m_board[2] == m_board[4] && m_board[4] == m_board[6])
				{
					m_winner = m_board[2];
				}
	//No match/Tie Game			
		else
				{
					m_winner = *"There is no winner!";
				}
	}


	bool IsOver()
	{
		//Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
		bool Over;

		if (m_numTurns == 10 || (m_winner == m_player1 || m_winner == m_player2))
		{
			Over = true;
		}
		else 
		{
			Over = false;
		}

		return Over;
	}

	
	char GetPlayerTurn()
	{
		/*Used switch case because in my version I have always done X goes firstand following that rule X will always be inputed at a certain "Turn number"
		which is found using the m_numTurns method */
		switch (m_numTurns)
		{

		case 1:
			m_playerTurn = m_player1;
			break;
		case 2:
			m_playerTurn = m_player2;
			break;
		case 3:
			m_playerTurn = m_player1;
			break;
		case 4:
			m_playerTurn = m_player2;
			break;
		case 5:
			m_playerTurn = m_player1;
			break;
		case 6:
			m_playerTurn = m_player2;
			break;
		case 7:
			m_playerTurn = m_player1;
			break;
		case 8:
			m_playerTurn = m_player2;
			break;
		case 9:
			m_playerTurn = m_player1;
			break;
		}
		
		return m_playerTurn;
	}
		
	
		bool IsValidMove(int position)
			{
			
			bool Valid;

			//checks if "position" is a valid space on the board and not occupied
			if (10 > position > 0 && (m_board[position-1] != m_player1 && m_board[position-1] != m_player2)) 
				{
					Valid = true;
					CheckWinner();
				}
				else
				{
					Valid=false;
					std::cout << "Please Enter A Valid Space\n";
				}
			return Valid;
			}

		
		void Move(int position)
		{
			//Sets the postion the player entered as the coresponding players character
			m_board[position - 1] = GetPlayerTurn();

			//After every move the turn number is increased by one using the following operation
			m_numTurns++;

			//After every move the game checks if there is a winner
			CheckWinner();
			
		}




		void DisplayResult()
		{
			//Displays a message stating who the winning player is, or if the game ended in a tie.
			if (m_winner == m_player1)
			{
				std::cout << "The Winner is Player One!\n";
			}
			else if (m_winner == m_player2)
			{
				std::cout << "The winner is Player Two\n";
			}
			else
			{
				std::cout << "The Game is a Tie!\n";
			}


		}
};








